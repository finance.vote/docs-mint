.. finance.vote documentation master file, created by
   sphinx-quickstart on Wed Jun 22 09:21:45 2022.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to mint's documentation!
========================================

`mint is`_  a merkle root based eligibility system for NFT issuance and price discovery. 

.. _mint is: https://mint.factorydao.org/fvt

.. image:: ./images/mint_hz_b.png
   :height: 50px
   :alt: docs-mint missing
   :align: center
   :target: https://mint.factorydao.org/fvt

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   /content/setting_up
   /content/contracts_deployment
   /content/createMint
   /content/1of1

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
  

Our products
===============

.. |L| image:: ./images/launch_icon.png
   :height: 15px
   :target: https://financevote.readthedocs.io/projects/launch/en/latest/
   
.. |B| image:: ./images/bank_icon_dark.png
   :height: 15px
   :target: https://financevote.readthedocs.io/projects/bank/en/latest/

.. |I| image:: ./images/influence_icon.png
   :height: 15px
   :target: https://financevote.readthedocs.io/projects/influence/en/latest/

.. |M| image:: ./images/markets_icon.png
   :height: 15px
   :target: https://financevote.readthedocs.io/projects/markets/en/latest/

.. |Y| image:: ./images/yield_icon.png
   :height: 15px
   :target: https://financevote.readthedocs.io/projects/yield/en/latest/

* |B| `bank <https://financevote.readthedocs.io/projects/bank/en/latest/>`_
* |I| `influence <https://financevote.readthedocs.io/projects/influence/en/latest/>`_
* |L| `launch <https://financevote.readthedocs.io/projects/launch/en/latest/>`_
* |M| `markets <https://financevote.readthedocs.io/projects/markets/en/latest/>`_
* |Y| `yield <https://financevote.readthedocs.io/projects/yield/en/latest/>`_



Back to main page
------------------
.. |H| image:: ./images/financevote_icon.png
   :height: 15px
   :target: https://financevote.readthedocs.io/en/latest/

* |H| `Home <https://financevote.readthedocs.io/en/latest/>`_