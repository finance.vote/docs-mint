Deploying smart contracts on testnet
=====================================
| To set up a mint, several contracts are needed to be deployed in the right order.

1. MerkleIdentity.sol::

    address _mgmt atribute -> creator wallet address

2. VoterID.sol::

    ooner atribute -> creator wallet address
    minter atribute -> MerkleIdentity contract address
    nomen atribute -> name of your choice
    symbowl atribute -> symbol of your choice

3. AmaluEligibility.sol or MerkleEligibility.sol::

    AmaluEligibility:
    _mgmt atribute -> creator wallet address
    _gatemaster atribute -> MerkleIdentity contract address

    MerkleEligibility:
    _gatemaster atribute -> MerkleIdentity contract address

4. AmaluPriceGate.sol or FixedPricePassThruGate.sol or SpeedBumpPriceGate.sol


.. attention::

    **NFT** address = **VoterId** contract address

    **GATE** address = **MerkleIdentity** contract address


