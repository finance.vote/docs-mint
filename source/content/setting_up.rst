Setting up
===========
Frontend
----------
.. _Mint: https://gitlab.com/finance.vote/factory-dao-mint.git

Repository link: `Mint`_

1. In the influence terminal::

    git checkout develop

2. Install dependencies::

    npm install

3. Run application::

    npm start


Backend
---------
.. _Mint backend: https://gitlab.com/finance.vote/minting-backend.git

Repository link: `Mint backend`_

In the minting-backend repo::

    nvm use
    npm i 
    docker-compose up --build